#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "ydlidarTask.h"
#include "esp_log.h"

static const char* TAG ="main";

//CAUTION: called in ydlidarTask
ydlidar_summary_t sum;
float dist[360];
uint8_t flag=0;
void onRecv(void *dummy){
    flag=1;
}
void app_main()
{
    static YDPacket data;
    ydlidar_begin(UART_NUM_2,17,16,onRecv,NULL);
    ESP_LOGI(TAG,"Start command is sent");
    while(!ydlidar_is_ready())vTaskDelay(1 / portTICK_PERIOD_MS);
    ESP_LOGI(TAG,"First packet comes! start!");
    for(;;){
        ////////////////////////
        //Get packet data exsample
        ///////////////////////
        for(int l=0;l<30;l++){
            if(flag){
                flag=0;
                ydlidar_get(&data);
                for(int i=0;i<data.lsn;i++){
                    int pos=(int)(ydpacket_angle(&data,i));
                    pos=(pos+360)%360;
                    dist[pos]=ydpacket_len(&data,i);
                }
            }
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }

        static char buf[1000];
        char *p=buf;
        for(int i=0;i<36;i++){
            
            float s=0;
            for(int j=0;j<10;j++)s+=dist[i*10+j];
            s/=10;
            p+=sprintf(p,"%d ",(int)s);
        }
        ESP_LOGI(TAG,"%s",buf);
        ESP_LOGI(TAG,"lastTick: %d",ydlidar_get_tickcount_from_last());
        //////////////////////
        //get summary exsample
        //////////////////////
        
        p=buf;
        ydlidar_get_summary(sum);
        for(int i=0;i<YDLIDAR_SUMMARY_LEN;i++){
            p+=sprintf(p,"%d ",(int)sum[i]);
        }
        ESP_LOGI(TAG,"%s",buf);

    }
    esp_restart();
}
